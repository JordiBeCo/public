 
    function activitat1(){
                        
                            //BORREM EL DIV QUE CONTE TOTA L'ACTIVITAT.
                            var div_principal = document.getElementById("divPrincipal");
                            div_principal.innerHTML="";
                           
                            //CREEM EL DIV CAIXA I L'AFEGIM DINS DEL BODY
                            var div_caixa = document.createElement("div");
                            div_caixa.setAttribute("id","caixa");
                            div_principal.appendChild(div_caixa);
                                //CREEM EL DIV HEADER I L'AFEGIM DINS DEL DIV CAIXA
                                var div_header = document.createElement("div");
                                div_header.setAttribute("id","header");
                                div_caixa.appendChild(div_header);
                                    //CREEM EL H2 CAIXA I L'AFEGIM DINS DEL DIV HEADER
                                    var h2 = document.createElement("h2");
                                    div_header.appendChild(h2);
                                        //CREEM EL TEXT I L'AFEGIM DINS DEL H2
                                        var text_h2 = document.createTextNode("Hola masters!");
                                        h2.appendChild(text_h2);
                                
                                
                                //CREEM EL DIV BODY I L'AFEGIM DINS DEL DIV CAIXA
                                var div_body = document.createElement("div");
                                div_body.setAttribute("id","body");
                                div_caixa.appendChild(div_body);
                                    //CREEM EL DIV_TEXT I L'AFEGIM DINS DEL DIV_BODY
                                    var div_text = document.createElement("div");
                                    div_text.setAttribute("id","text");
                                    div_body.appendChild(div_text);
                                        //CREEM EL TEXT I L'AFEGIM DINS DEL DIV_TEXT
                                        var paragraf_1 = document.createTextNode("Missatge pels de 2n DAW");
                                        div_text.appendChild(paragraf_1);
                                                    
                                    //CREEM EL DIV MESTEXT I L'AFEGIM DINS DE DIV_BODY
                                    var div_mestext = document.createElement("div");
                                    div_mestext.setAttribute("id","anothertext");
                                    div_caixa.appendChild(div_mestext);
                                        //CREEM EL PARAGRAF P1 I LL'AFEGIM DINS DEL DIV_MESTETX
                                        var p1 = document.createElement("p");
                                        div_mestext.appendChild(p1);
                                            //CREEM UN TEXT I L'AFEGIM DINS DE P1
                                            var text_p1 = document.createTextNode("Avui serà un dia meravellós...");
                                            p1.appendChild(text_p1);
                                        
                                        //CREEM EL PARAGRAF P2 I LL'AFEGIM DINS DEL DIV_MESTETX
                                        var p2 = document.createElement("p");
                                        div_mestext.appendChild(p2);
                                            //CREEM UN TEXT I L'AFEGIM DINS DE P2
                                            var text_p2 = document.createTextNode("si ens ho proposem :)");
                                            p2.appendChild(text_p2);
                        
                            
                                //CREEM EL DIV FOOTER I L'AFEGIM DINS DEL DIV_CAIXA
                                var div_footer = document.createElement("div");
                                div_footer.setAttribute("id","footer");
                                div_caixa.appendChild(div_footer);
                                    //CREEM UN PARAGRAF I L'AFEGIM DINS DEL FOOTER
                                    var p3 = document.createElement("p");
                                    div_footer.appendChild(p3);
                                        //CREEM EL TEXT I L'AFEGIM DINS DE P3
                                        var text_p3 = document.createTextNode("copyright ");
                                        p3.appendChild(text_p3);
                                        
                                        //CREEM LA NEGRETA I L'AFEGIM DINS DEL P3    
                                        var negreta = document.createElement("b");
                                        p3.appendChild(negreta);
                                            //CREEM TEXT I L'AFEGIM DINS DE NEGRETA;
                                            var text_negrata = document.createTextNode("2017");
                                            negreta.appendChild(text_negrata);
                                    
                }

    function activitat2(){
        //BORREM EL DIV QUE CONTE TOTA L'ACTIVITAT.
        var div_principal = document.getElementById("divPrincipal");
        div_principal.innerHTML="";
        
        var Elements = ["div","h2","p","a","h1"];
        
        for(var i = 0; i<Elements.length; i++){
            
            
                    var tipus = document.createElement(Elements[i]);
                    div_principal.appendChild(tipus);
                        
                        var text = document.createTextNode("Lorem ipsum Prova 1,2,3,4");
                        tipus.appendChild(text);
        
        }
    }

    function activitat3(){
        //BORREM EL DIV QUE CONTE TOTA L'ACTIVITAT.
        var div_principal = document.getElementById("divPrincipal");
        div_principal.innerHTML="";
        
        
            //CREEM EL DIV HEADER I L'AFEGIM DINS DEL DIV CAIXA I EL PINTEM DE COLOR BLAU
                    var div_contenidor = document.createElement("div");
                    div_contenidor.setAttribute("style","background-color: blue;");
                    div_principal.appendChild(div_contenidor);
        
        //ARRAY AMB ELS ELEMENTS A CREAR
        var Elements = ["div","h2","p","a","h1"];
        
        //BUCLE PER CREAR TOTS ELS ELEMENTS DE DINS DE L'ARRAY
        for(var i = 0; i<Elements.length; i++){
                    var tipus = document.createElement(Elements[i]);
                    div_contenidor.appendChild(tipus);
                        
                        var text = document.createTextNode("Lorem ipsum Prova 1,2,3,4");
                        tipus.appendChild(text);
        
        }
    }

    function activitat4(){
       //BORREM EL DIV QUE CONTE TOTA L'ACTIVITAT.
            var div_principal = document.getElementById("divPrincipal");
            div_principal.innerHTML=""; 
        
        //CREEM EL DIV DEL TAULER D'ESCACS
        var div_tauler = document.createElement("div");
        div_principal.appendChild(div_tauler);
        div_tauler.setAttribute("id" , "taulerEscacs");
        div_tauler.setAttribute("style" , "width: 320px; height: 320px; border: 1px solid black");
        
        
        //CREEM LES CEL·LES DEL TAULER UNA A UNA AMB UN FOR ANIDAT
        for(var i = 0; i < 8; i++){
            for(var j = 0; j<8;j++){
                var div_casella = document.createElement("div");
                div_tauler.appendChild(div_casella);
                div_casella.setAttribute("id" , "casellaEscacs");
                
        //SI LA SUMA DE J+I ÉS PARELL VOL DIR QUE LA CEL·LA ÉS BLANCA. SI NO ES AIXI ES NEGRE.         
                if((j+i)%2 == 0){
                div_casella.setAttribute("style" , "float: left; background-color: white; width: 40px; height: 40px");
                }
                else{div_casella.setAttribute("style" , "float: left; background-color: black; width: 40px; height: 40px");}
            }
        }
        
        
                        
    }

    function activitat5(){
        //BORREM EL DIV QUE CONTE TOTA L'ACTIVITAT.
        var div_principal = document.getElementById("divPrincipal");
        div_principal.innerHTML="";
        
        var Elements = ["Codi_de_l'article","Descripció","Preu_Unitari","Unitats","Descompte_en_%"];
        
    
        //-----------PINTAR EL DIV DEL FORMULARI------------------
        var div_formulari = document.createElement("div");
        div_principal.appendChild(div_formulari);
        div_formulari.setAttribute("style", "border: 1px solid black; width: 220px; float: left; margin-left: 20px");
        
            
            //-------CREAR ELS CAMPS DEL  DEL FORMULARI-----
            for (var i =0; i < Elements.length; i++){
            
            var p1 = document.createElement("p");
            div_formulari.appendChild(p1);
            var text1 = document.createTextNode(Elements[i]);
            p1.appendChild(text1);
            
            var imput1 = document.createElement("input");
            div_formulari.appendChild(imput1);
            imput1.setAttribute("id", Elements[i]);
            
            }
            
            //CREEM EL BOTO D'ENVIAER EL FORMULARI
            var button1 = document.createElement("button");
            div_formulari.appendChild(button1);
            var textbutton1 = document.createTextNode("Submit");
            button1.appendChild(textbutton1);
            button1.setAttribute("id", "SubmitButton");
            button1.setAttribute("type", "button");
            button1.setAttribute("style", "margin-top: 15px");
            button1.setAttribute("onclick", "funcio5_2()");
            
        
            //CREEM EL BOTO DE NETEJAR EL FORMULARI:
            var button2 = document.createElement("button");
            div_formulari.appendChild(button2);
            var textbutton2 = document.createTextNode("Clear");
            button2.appendChild(textbutton2);
            button2.setAttribute("id", "SubmitButton");
            button2.setAttribute("type", "button");
            button2.setAttribute("style", "margin-top: 15px; margin-left: 10px");
            button2.setAttribute("onclick", "funcio5_3()");
        
            
            //CREEM EL BOTO DE CALCULAR EL PREU AMB IVA ANDORRÀ:
            var button3 = document.createElement("button");
            div_formulari.appendChild(button3);
            var textbutton3 = document.createTextNode("Calcular IVA");
            button3.appendChild(textbutton3);
            button3.setAttribute("id", "SubmitButton");
            button3.setAttribute("type", "button");
            button3.setAttribute("style", "margin-top: 15px; margin-left: 10px");
            button3.setAttribute("onclick", "funcio5_4()");
            
            
            
        
        //------------PINTAR LA TAULA------------------------
        
            var div_taula = document.createElement("div");
            div_principal.appendChild(div_taula);
            div_taula.setAttribute("style", "margin-left: 20px; float: left");
            
        
                var taula = document.createElement("table");
                div_taula.appendChild(taula);
                taula.setAttribute("id", "taulaId");
                
                    var primera_fila = document.createElement("tr");
                    taula.appendChild(primera_fila);
                        
               
                    for (var i = 0; i < Elements.length; i++){
                        var th = document.createElement("th");
                        primera_fila.appendChild(th);
                        th.setAttribute("style", "border: 1px solid black");
                            
                            var text = document.createTextNode(Elements[i]);
                            th.appendChild(text);
                    }
            
        
    }

    function funcio5_2(){
        
        //------------PINTAR LA TAULA------------------------
        
            //OBTENIM ELS VALORS QUE HA INTRODUÏT L'USUARI I ELS GUARDEM EN VARIABLES.
            var codi = document.getElementById("Codi_de_l'article").value;
            var descripcio = document.getElementById("Descripció").value;
            var preu = document.getElementById("Preu_Unitari").value;
            var unitats = document.getElementById("Unitats").value;
            var descompte = document.getElementById("Descompte_en_%").value;
        
            var variables = [codi,descripcio,preu,unitats,descompte];
            
            //OBTENIM LA TAULA PER PODER CREAR FILES AMB ELS VALORS DE L'USUARI.
            var taula = document.getElementById("taulaId");
            
            //CREEM UNA FILA A LA TAULA ON INSERTAREM LES MOVES DADES.
            var fila = document.createElement("tr");
            taula.appendChild(fila);
            
            //BUCLE AMB EL QUAL OMPLIREM LA FILA AMB ELS VALORS QUE HA ENTRAT L'USUARI.
            for(var i = 0; i < variables.length; i++){
                var td = document.createElement("td");
                fila.appendChild(td);
                td.setAttribute("style", "border: 1px solid black");
                
                var text = document.createTextNode(variables[i]);
                td.appendChild(text);
            }
            
            
    }

    function funcio5_3(){
            //VUIDEM EL CONTINGUT DELS IMPUTS DEL FORMULARI.
            document.getElementById("Codi_de_l'article").value ="";
            document.getElementById("Descripció").value ="";
            document.getElementById("Preu_Unitari").value ="";
            document.getElementById("Unitats").value ="";
            document.getElementById("Descompte_en_%").value ="";

   
    }

    function funcio5_4(){
            //AGAFEM ELS VALORS NECESSARIS PER CALCULAR EL PREU AMB IVA I DESCOMPTE I EL CALCULEM:
            var descompte = document.getElementById("Descompte_en_%").value;
            var preu = document.getElementById("Preu_Unitari").value;
            var preuAmbIva = preu * 1.045;
            var descompte2 = ((100 - descompte)/100);
            
            var preuFinal = preuAmbIva * descompte2;
            
            alert(preuAmbIva);
    }
